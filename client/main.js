import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
});

Template.hello.helpers({
  counter() {
    return Template.instance().counter.get();
  },
});

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
});

reallySimpleWeather.weather({
    wunderkey: '', //add your key
    location: 'Cape Town, OR',
    woeid: '',
    unit: '°',
    success: function(weather) {
      html = '<h2>'+weather.temp+'°' + 'F</h2>';
    html += '<ul><li>'+weather.full + '</li>';
      html += '<li>'+weather.currently+'</li>';
    html += '<li>'+weather.wind.direction+' '+weather.wind.speed+' mph</li></ul>';
    document.getElementById('weather').innerHTML = html;
    },
    error: function(error) {
    document.getElementById('weather').innerHTML = '<p>'+error+'</p>';
    }
});

 $( "h2" ).remove( ":contains('°F')" );
